angular.module('miniCouch', ['ngCookies'])
.controller('registerController', function($scope, $http, $cookies) {
	
	//init
	$scope.registerErrorMessage = "";
	$scope.hideRegisterFrom = false;
	$scope.showRegisterSuccess = false;
	
	console.log("current cookie:");
	console.log($cookies.get("miniCouchRegisterToken"));


	//Submits a registerrequest to the backend.
	//If success: show success message and set cookie
	//If error: show error
	submitRegisterRequest = function(email, pass, first_name, surname, initials) {
	
		var req = { // request
			method : 'POST',
			url : "http://localhost:8080/register", //CHECK PORT!
			data : Object.toparams({
				'email':email, 
				'password':pass,
				'first_name':first_name,
				'surname':surname,
				'initials':initials
			}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		
		$http(req)
			.then(function successCallback(response) {
				console.log(response.data);
				if (response.data.code == 200) {
				//@JP: There is no token returned when registering!
				
				//	var token = response.data.data;
				//	var cookieExpireDate = new Date();
				//	cookieExpireDate.setDate(cookieExpireDate.getDate() + 1);
				//	//Set cookie:
				//	$cookies.put("miniCouchRegisterToken", token, {'expires': cookieExpireDate} );
					
					//Show success message:
					$scope.hideRegisterFrom = true;
					$scope.showRegisterSuccess = true;
					
					$scope.registerErrorMessage = "OK!";

				}
				else {
					$scope.registerErrorMessage = response.data.error;
				}
			}, function errorCallback(response) {
				console.log(response.data);
				$scope.registerErrorMessage = response.data.error;
			}
		);
			
			
			
	}
	
	//Try to register. (Can be submitted by front-end)
	$scope.tryRegister = function() {
		console.log($scope.email);
		var email = $scope.email;
		var registerPass = $scope.registerPass;
		var first_name = $scope.first_name;
		var surname = $scope.surname;
		var initials = $scope.initials;
		
		submitRegisterRequest(email, registerPass, first_name, surname, initials);
		
	}
});

//https://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
Object.toparams = function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
};