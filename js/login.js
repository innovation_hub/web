angular.module('miniCouch', ['ngCookies'])
.controller('loginController', function($scope, $http, $cookies) {
	
	//init
	$scope.loginErrorMessage = "";
	$scope.hideLoginFrom = false;
	$scope.showLoginSuccess = false;
	
	console.log("current cookie:");
	console.log($cookies.get("miniCouchLoginToken"));


	//Submits a loginrequest to the backend.
	//If success: show success message and set cookie
	//If error: show error
	submitLoginRequest = function(loginId, pass) {
	
		var req = { // request
			method : 'POST',
			url : "http://localhost:8080/login", //CHECK PORT!
			data : Object.toparams({
				'email':loginId, //thijseckhart@gmail.com
				'password':pass //123456
			}),
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
		}
		
		$http(req)
			.then(function successCallback(response) {
				console.log(response.data);
				if (response.data.code == 200) {
					var token = response.data.data;
					var cookieExpireDate = new Date();
					cookieExpireDate.setDate(cookieExpireDate.getDate() + 1);
					//Set cookie:
					$cookies.put("miniCouchLoginToken", token, {'expires': cookieExpireDate} );
					
					//Show success message:
					$scope.hideLoginFrom = true;
					$scope.showLoginSuccess = true;
					
					$scope.loginErrorMessage = "OK!";

				}
				else {
					$scope.loginErrorMessage = response.data.error;
				}
			}, function errorCallback(response) {
				console.log(response.data);
				$scope.loginErrorMessage = response.data.error;
			}
		);
			
			
			
	}
	
	//Try to login. (Can be submitted by front-end)
	$scope.tryLogin = function() {
		console.log($scope.loginId);
		var loginId = $scope.loginId;
		var loginPass = $scope.loginPass;
		
		submitLoginRequest(loginId, loginPass);
		
	}
});

//https://stackoverflow.com/questions/19254029/angularjs-http-post-does-not-send-data
Object.toparams = function ObjecttoParams(obj) {
    var p = [];
    for (var key in obj) {
        p.push(key + '=' + encodeURIComponent(obj[key]));
    }
    return p.join('&');
};